--<<
_ = wesnoth.textdomain 'wesnoth-tune'

if rawget(_G, 'tune') == nil then
  wesnoth.interface.add_chat_message "declaring lua functions"
  tune = {}
  tune.fun = {}
end

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function chump(o)
  wesnoth.interface.add_chat_message(dump(o))
end

function tune.fun.correct_xp_1(unit, sunit)
  local unitm, sunitm

  if sunit.hitpoints > 0 then
    unitm = sunit.level
  else
    if sunit.level == 0 then
      unitm = 4
    else
      unitm = sunit.level * 8
    end
  end
  -- wesnoth.interface.add_chat_message(unit.id .. ": " .. unit.experience .. " to " .. (unit.experience + unitm))
  return unit.experience + unitm
end

function tune.fun.correct_xp_2(unit, sunit)
  if unit.hitpoints > 0 then
    sunitm = unit.level
  else
    if unit.level == 0 then
      sunitm = 4
    else
      sunitm = unit.level * 8
    end
  end
  -- wesnoth.interface.add_chat_message(sunit.id .. "s: " .. sunit.experience .. " to " .. (sunit.experience + sunitm))
  return sunit.experience + sunitm
end

-->>
