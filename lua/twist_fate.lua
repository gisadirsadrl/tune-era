-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

local units_to_untwist = wesnoth.units.find_on_map({side=wesnoth.current.side,status="lucky_tune"})
for _,u in ipairs(units_to_untwist) do
	u:remove_modifications({id="lucky_tune"})
	wesnoth.wml_actions.remove_unit_overlay{id=u.id, image="misc/lucky.png"}
	u.status.lucky_tune = false
end

local units_to_twist = wesnoth.units.find_on_map({T.filter_adjacent{ability_type="twist_fate_tune",is_enemy="no",side=wesnoth.current.side},{"not",{status="lucky_tune"}},{"not",{status="not_living"}}})

for _,u in ipairs(units_to_twist) do
	u:add_modification("object",{
		id="lucky_tune",
		T.effect{
			apply_to="overlay",
			add="misc/lucky.png",
		},
		T.effect{
			apply_to="new_ability",
			T.abilities{
				T.chance_to_hit{
					id="lucky tune",
					apply_to="opponent",
					value=-100,
				},
				T.chance_to_hit{
					id="lucky tune",
					apply_to="opponent",
					value=-0,
				},
			},
		},
	})
	u.status.lucky_tune = true
			
	-- wesnoth.audio.play("entangle.wav")
end

-->>