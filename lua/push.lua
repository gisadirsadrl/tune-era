-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

function pushat(thlx,thly,dir)
  -- for _,unit in ipairs(wesnoth.units.find_on_map({x=thlx,y=thly,T.filter_location{{"not",{terrain="*^V*"}}}})) do
  for _,unit in ipairs(wesnoth.units.find_on_map({x=thlx,y=thly})) do
    -- chump(unit)
    local th = wesnoth.map.get_direction(unit.loc,dir)
      if wesnoth.current.map:on_board(th) and (not wesnoth.map.matches(th.x,th.y,{terrain = "X*^*"})) then
        for __,bracing_unit in ipairs(wesnoth.units.find_on_map({x=th.x,y=th.y})) do
          -- chump("braced by:")
          -- chump(bracing_unit)
          pushat(th.x,th.y,dir)
          -- chump("finished pushing")
        end
        if (#wesnoth.units.find_on_map({x=th.x,y=th.y}) == 0) then
          wml.fire("move_unit", { x = thlx, y = thly, dir = dir, check_passability = "no", fire_event = "yes"})
          if unit.id == wml.variables["unit"].id then
            unit.experience = x1
          end
          if unit.id == wml.variables["second_unit"].id then
            unit.experience = x2
          end
          if unit.hitpoints <= 0 then
            wml.fire("kill", { x = th.x, y = th.y, fire_event = "yes"})
          else
            wesnoth.map.set_owner(th.x,th.y,unit.side)
          end
        end
      end
    end
end

if wml.variables["push_hits_tune"] >= 1 then
  for _,u in ipairs(wesnoth.units.find_on_map({id=wml.variables["unit"].id})) do
    for _,u2 in ipairs(wesnoth.units.find_on_map({id=wml.variables["second_unit"].id})) do
      x1 = tune.fun.correct_xp_1(u,u2)
      x2 = tune.fun.correct_xp_2(u,u2)
      local dir = u.facing
      pushat(u.x,u.y,dir)
    end
  end
end