-- wesnoth.interface.add_chat_message "declarisra:"

local T = wml.tag

local units = wesnoth.units.find_on_map({id=wml.variables["blind_victim"].id})
for _,u in ipairs(units) do
	if u.hitpoints > 0 then
		if not(u.status.blinded_tune) then
			u:add_modification("object",{
				id="blinded_tune",
				T.effect{
					apply_to="overlay",
					add="misc/blinded.png",
				},
				T.effect{
					apply_to="vision",
					set=0,
				},
				T.effect{
					apply_to="attack",
					{"not",{
						special_id="magical"
					},},
					T.set_specials{
						mode="append",
						T.chance_to_hit{
							id="blinded tune",
							apply_to="self",
							divide=2,
							cumulative=false,
						},
					},
				},
			})
			u.status.blinded_tune = true
		else
			u:add_modification("object",{
				id="double_blinded_tune",
				T.effect{
					apply_to="overlay",
					add="misc/no_overlay.png~BLIT(misc/blinded.png,0,9)",
				},
			})
			u.status.double_blinded_tune = true
		end
		
		wesnoth.audio.play("pincers.ogg")
	end
end

-->>