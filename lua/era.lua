--<<

local T = wml.tag

local res = {}

res.mp_leader_speed = function(args)
	local trait_quick = args[1][2]
	local trait_slow = args[2][2]
	
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true }) do
		unit:add_modification("object", {T.effect{apply_to="movement_costs",replace=true,T.movement_costs{castle=1,cave=2,reef=2,deep_water=100,flat=1,forest=2,frozen=3,fungus=2,hills=2,mountains=3,sand=2,shallow_water=3,swamp=3,unwalkable=100,village=1}}} )
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = 4 } }) do
		unit:add_modification("trait", trait_quick )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = "10" } }) do
		unit:add_modification("trait", trait_slow )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = "9" } }) do
		unit:add_modification("trait", trait_slow )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = "8" } }) do
		unit:add_modification("trait", trait_slow )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = "7" } }) do
		unit:add_modification("trait", trait_slow )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
	for i, unit in ipairs(wesnoth.units.find_on_map { canrecruit = true, T.filter_wml { max_moves = "6" } }) do
		unit:add_modification("trait", trait_slow )
		unit.moves = unit.max_moves
		unit.hitpoints = unit.max_hitpoints
	end
end
return res

-->>
