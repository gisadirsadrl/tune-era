-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

if wml.variables["plague_flowtoggle_tune"] then
  for _,u in ipairs(wesnoth.units.find_on_map({x=wesnoth.current.event_context.x1,y=wesnoth.current.event_context.y1})) do
  -- for _,u in ipairs(wesnoth.units.find_on_map({})) do
    local traithaving = false
    for modi in wml.child_range(u.__cfg, "modifications") do
    	for trai in wml.child_range(modi, "trait") do
        if not (trai.availability == "musthave") then
          -- wesnoth.interface.add_chat_message(trai.id)
          u:remove_modifications({id=trai.id}, "trait")
          traithaving = true
        end
      end
  	end
    if traithaving then
      -- wesnoth.interface.add_chat_message("found someone to apply traits to: " .. u.type .. " at " .. tostring(u.x) .. ", " .. tostring(u.y))
      for modi in wml.child_range(wml.variables["dead_unit_tune"], "modifications") do
        -- wesnoth.interface.add_chat_message("modifications ")
      	for trai in wml.child_range(modi, "trait") do
          -- wesnoth.interface.add_chat_message("trait " .. trai.id)
          if not (trai.availability == "musthave") then
            -- wesnoth.interface.add_chat_message(dump(trai))
            u:add_modification("trait",trai)
          end
        end
    	end
    end
    u.hitpoints = u.max_hitpoints
  end
  wml.variables["plague_flowtoggle_tune"] = false
end

  
--   if u.hitpoints > 0 then
--     local dir = "-" .. u.facing  
--     local th = wesnoth.map.get_direction(u.loc,dir)
--     if wesnoth.current.map:on_board(th) then
--       path, cost = wesnoth.paths.find_path(u, th.x, th.y)
--       if cost < 101 then
--         if #wesnoth.units.find_on_map({x=th.x,y=th.y}) == 0 then
--           wml.fire("move_unit", { x = u.x, y = u.y, dir = dir, check_passability = "no", fire_event = "yes"})
--           for __,u2 in ipairs(wesnoth.units.find_on_map({x=wesnoth.current.event_context.x2,y=wesnoth.current.event_context.y2})) do
--             if u2.hitpoints > 0 then
--               wml.fire("move_unit", { x = u2.x, y = u2.y, dir = dir, check_passability = "no", fire_event = "yes"})
--             end
--           end
--         end
--       end
--     end
--   end
-- end
