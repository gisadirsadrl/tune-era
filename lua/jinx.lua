-- wesnoth.interface.add_chat_message "declarisra:"

local T = wml.tag

local function sign(x)
	if x > 0 then return 1 end
	return -1
end

local abs=math.abs

-- wml.variables["pls"] = wml.variables["weapon"]

-- console testscript
-- for spec in wml.child_range(wml.variables["pls"], "specials") do for jx in wml.child_range(spec, "jinx") do print(jx.value) end end

local def_redu = 0
local max_cth = 0

for spec in wml.child_range(wml.variables["jinx_weapon"], "specials") do
	for jx in wml.child_range(spec, "jinx_tune") do
		def_redu = jx.value
		max_cth = jx.max_value
	end
end

local function map_def(defense)
	if not defense then return 100 end
	if abs(defense) > max_cth then return defense end
	if abs(defense) > (max_cth-def_redu) then return sign(defense)*max_cth end
	return sign(defense)*(abs(defense)+def_redu)
end

local units = wesnoth.units.find_on_map({id=wml.variables["jinx_victim"].id})
for _,u in ipairs(units) do
	if u.hitpoints > 0 then
		local def = wml.get_child(u.__cfg, "defense")
	
		u:add_modification("object",{
			id="jinx_tune",
			T.effect{
				apply_to="overlay",
				add="misc/jinxed.png",
			},
			T.effect{
				apply_to="defense",
				replace=true,
				T.defense{
					deep_water=map_def(def["deep_water"]),
					shallow_water=map_def(def["shallow_water"]),
					reef=map_def(def["reef"]),
					swamp_water=map_def(def["swamp_water"]),
					flat=map_def(def["flat"]),
					sand=map_def(def["sand"]),
					forest=map_def(def["forest"]),
					hills=map_def(def["hills"]),
					mountains=map_def(def["mountains"]),
					village=map_def(def["village"]),
					castle=map_def(def["castle"]),
					cave=map_def(def["cave"]),
					frozen=map_def(def["frozen"]),
					unwalkable=map_def(def["unwalkable"]),
					impassable=map_def(def["impassable"]),
					fungus=map_def(def["fungus"]),
				}
			},
		})
		u.status.jinxed_tune = true
		
		wesnoth.audio.play("gold.ogg")
	end
end

-->>