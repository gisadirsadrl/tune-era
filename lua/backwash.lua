-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

if wml.variables["backwash_hits_tune"] >= 1 then
  for _,u in ipairs(wesnoth.units.find_on_map({id=wml.variables["unit"].id})) do
    for _,u2 in ipairs(wesnoth.units.find_on_map({id=wml.variables["second_unit"].id})) do
      x1 = tune.fun.correct_xp_1(u,u2)
      x2 = tune.fun.correct_xp_2(u,u2)
      local dir = "-" .. u.facing
      local th = wesnoth.map.get_direction(u.loc,dir)
      if wesnoth.current.map:on_board(th) and (not wesnoth.map.matches(th.x,th.y,{terrain = "X*^*"})) and (#wesnoth.units.find_on_map({x=th.x,y=th.y}) == 0) then
        wml.fire("move_unit", { x = u.x, y = u.y, dir = dir, check_passability = "no", fire_event = "yes"})
        u.experience = x1
        if u.hitpoints <= 0 then
          wml.fire("kill", { x = th.x, y = th.y, fire_event = "yes"})
        else
          wesnoth.map.set_owner(th.x,th.y,u.side)
        end
      end
      local th = wesnoth.map.get_direction(u2.loc,dir)
      if wesnoth.current.map:on_board(th) and (not wesnoth.map.matches(th.x,th.y,{terrain = "X*^*"})) and (#wesnoth.units.find_on_map({x=th.x,y=th.y}) == 0) then
        wml.fire("move_unit", { x = u2.x, y = u2.y, dir = dir, check_passability = "no", fire_event = "yes"})
        u2.experience = x2
        if u2.hitpoints <= 0 then
          wml.fire("kill", { x = th.x, y = th.y, fire_event = "yes"})
        else
          wesnoth.map.set_owner(th.x,th.y,u2.side)
        end
      end
    end
  end
end