-- wesnoth.interface.add_chat_message "declarisra:"

local T = wml.tag

local units = wesnoth.units.find_on_map({id=wml.variables["root_victim"].id})
for _,u in ipairs(units) do
	if u.hitpoints > 0 then
		u:add_modification("object",{
			id="root_tune",
			T.effect{
				apply_to="overlay",
				add="misc/rooted.png",
			},
		})
		u.status.rooted_tune = true
		
		wesnoth.audio.play("sling.ogg")
	end
end

-->>