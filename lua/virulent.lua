-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

local units = wesnoth.units.find_on_map({T.filter_adjacent{ability_type="virulent_tune",side=wesnoth.current.side},{"not",{status="poisoned"}},{"not",{status="not_living"}}})

for _,u in ipairs(units) do
	u.status.poisoned = true
			
	wesnoth.audio.play("poison.ogg")
	wesnoth.audio.play("hiss.wav")
	wesnoth.audio.play("hiss-big.wav")
end

-->>