wesnoth.interface.add_chat_message("declarisra:")


local old_unit_status = wesnoth.interface.game_display.unit_status

function wesnoth.interface.game_display.unit_status()
	local u = wesnoth.interface.get_displayed_unit()
	if not u then return {} end
	local s = old_unit_status()

	if u.status.lucky_tune then
		table.insert(s, { "element", { image = "misc/lucky-status-icon.png",
			tooltip = _ "lucky: This unit is lucky and is assured to dodge the next blow."
		} } )
	end
	if u.status.jinxed_tune then
		table.insert(s, { "element", { image = "misc/jinxed-status-icon.png",
			tooltip = _ "jinxed: This unit is jinxed and gets a 13% defence penalty on every terrain until it advances."
		} } )
	end
	if u.status.rooted_tune then
		table.insert(s, { "element", { image = "misc/rooted-status-icon.png",
			tooltip = _ "rooted: This unit will be unable to move on its next turn!"
		} } )
	end
	if u.status.blinded_tune then
		table.insert(s, { "element", { image = "misc/blinded-status-icon.png",
			tooltip = _ "blinded: This unit will be unable to see and have an extra 50% chance to miss until it recovers at end of turn."
		} } )
	end
  
	return s
end

-->>
