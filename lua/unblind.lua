-- wesnoth.interface.add_chat_message("declarisra:")

local T = wml.tag

local units = wesnoth.units.find_on_map({side=wesnoth.current.side,status="blinded_tune"})

for _,u in ipairs(units) do
	if not(u.status.double_blinded_tune) then
		u:remove_modifications({id="blinded_tune"})
		wesnoth.wml_actions.remove_unit_overlay{id=u.id, image="misc/blinded.png"}
		u.status.blinded_tune = false
	else
		u:remove_modifications({id="double_blinded_tune"})
		wesnoth.wml_actions.remove_unit_overlay{id=u.id, image="misc/no_overlay.png~BLIT(misc/blinded.png,0,9)"}
		u.status.double_blinded_tune = false
	end
	
			
	-- wesnoth.audio.play("heal.wav")
end

-->>